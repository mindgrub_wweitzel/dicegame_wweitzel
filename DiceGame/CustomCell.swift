//
//  CustomCell.swift
//  DiceGame
//
//  Created by Wesley Weitzel on 7/1/16.
//  Copyright © 2016 Wesley Weitzel. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var playerScore: UILabel!
    
    @IBOutlet weak var computerScore: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
