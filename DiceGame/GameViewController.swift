//
//  ViewController.swift
//  DiceGame
//
//  Created by Wesley Weitzel on 6/29/16.
//  Copyright © 2016 Wesley Weitzel. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    
    @IBOutlet weak var turnTotal: UILabel!
    @IBOutlet private weak var playerScore: UILabel!
    @IBOutlet weak var computerScore: UILabel!
    @IBOutlet weak var diceImage: UIImageView!
    
    @IBOutlet weak var viewScoresButton: UIButton!
    @IBOutlet weak var newGameButton: UIButton!
    @IBOutlet weak var rollButton: UIButton!
    @IBOutlet weak var holdButton: UIButton!
    
    // Rolling this value will reset the turnTotal and change turn to the other player
    private let badRollValue = 1
    private let holdValueChance = 5
    private let showScoresSegIdentifier = "ShowScoresSeg"
    private let scoresTitle = "Scores"
    private let dice1Name = "dice1"
    private let dice2Name = "dice2"
    private let dice3Name = "dice3"
    private let dice4Name = "dice4"
    private let dice5Name = "dice5"
    private let dice6Name = "dice6"
    
    private var logic = Logic()
    private var scores = ScoresHandler()
    
    override func viewDidLoad() {
        viewScoresButton.setTitle(NSLocalizedString("View Scores", comment: ""), forState: .Normal)
        newGameButton.setTitle(NSLocalizedString("New Game", comment: ""), forState: .Normal)
        rollButton.setTitle(NSLocalizedString("Roll", comment: ""), forState: .Normal)
        holdButton.setTitle(NSLocalizedString("Hold", comment: ""), forState: .Normal)
    }
    
    /*
    Button touches
    */
    @IBAction func touchRoll(sender: UIButton) {
        roll()
    }
    
    @IBAction func touchHold(sender: UIButton) {
        hold()
    }
    
    @IBAction func touchNewGame(sender: UIButton) {
        reset()
    }
    
    private func roll() {
        let rollValue = Int(arc4random_uniform(6) + 1)
        setImage(rollValue)
        if (rollValue == badRollValue) {
            setTurnTotal(0)
            changeTurn()
        }
        else {
            setTurnTotal(logic.getTurnTotal() + rollValue)
        }
    }
    
    private func hold() {
        setPlayerScore()
        setTurnTotal(0)
        if (logic.getPlayerScore() >= logic.getGoalScore()) {
            endGame()
        } else {
            changeTurn()
        }
        
    }

    private func computerHold() {
        setComputerScore()
        setTurnTotal(0)
        if (logic.getComputerScore() >= logic.getGoalScore()) {
            endGame()
        }
        logic.setIsPlayerTurn(!logic.getIsPlayerTurn())
        setButtonStates()
    }
    
    /*
     Segue preparation for the view scores button
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == showScoresSegIdentifier {
            if let ivc = segue.destinationViewController as? UITableViewController {
                ivc.title = scoresTitle
            }
        }
    }
    
    private func computerTurn() {
        let priority = QOS_CLASS_USER_INITIATED
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            NSThread.sleepForTimeInterval(1)
            while (!self.logic.getIsPlayerTurn()) {
                let holdValue = Int(arc4random_uniform(UInt32(self.holdValueChance)))
                if (!(self.logic.getComputerScore() + self.logic.getTurnTotal() >= self.logic.getGoalScore()) && (holdValue != 1)) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.roll()
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.computerHold()
                    }
                }
                NSThread.sleepForTimeInterval(1)
            }
        }
    }
    
    private func endGame() {
        var message: String = ""
        if (!logic.getIsPlayerTurn()) {
            message += NSLocalizedString("Computer wins", comment: "") + String(logic.getComputerScore()) + NSLocalizedString("to", comment: "") + String(logic.getPlayerScore())
        } else {
            message += logic.getPlayerName() + NSLocalizedString("wins", comment: "") + String(logic.getPlayerScore()) + NSLocalizedString("to", comment: "") + String(logic.getComputerScore())
        }
        let alert = UIAlertController(title: NSLocalizedString("Game Over!", comment: ""), message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        let pScore = String(logic.getPlayerScore())
        let cScore = String(logic.getComputerScore())
        scores.saveItem(pScore, compScore: cScore)
        reset()
    }
    
    private func setTurnTotal(value: Int) {
        logic.setTurnTotal(value)
        turnTotal.text = String(NSLocalizedString("Turn Total : ", comment: "") + String(logic.getTurnTotal()))
    }
    
    private func setPlayerScore() {
        logic.setPlayerScore(logic.getTurnTotal() + logic.getPlayerScore())
        playerScore.text = String(logic.getPlayerName() + NSLocalizedString(" Score : ", comment: "") + String(logic.getPlayerScore()))
    }
    
    private func setComputerScore() {
        logic.setComputerScore(logic.getComputerScore() + logic.getTurnTotal())
        computerScore.text = String(NSLocalizedString("Computer Score: ", comment: "") + String(logic.getComputerScore()))
    }
    
    private func setImage(rollValue: Int) {
        switch rollValue {
        case 1: diceImage.image = UIImage(named: dice1Name)
        case 2: diceImage.image = UIImage(named: dice2Name)
        case 3: diceImage.image = UIImage(named: dice3Name)
        case 4: diceImage.image = UIImage(named: dice4Name)
        case 5: diceImage.image = UIImage(named: dice5Name)
        case 6: diceImage.image = UIImage(named: dice6Name)
        default: diceImage.image = UIImage(named: dice1Name)
        }
    }
    
    private func changeTurn() {
        logic.setIsPlayerTurn(!logic.getIsPlayerTurn())
        setButtonStates()
        if !logic.getIsPlayerTurn() {
            computerTurn()
        }
    }
    
    private func setButtonStates() {
        holdButton.enabled = logic.getIsPlayerTurn()
        rollButton.enabled = logic.getIsPlayerTurn()
        newGameButton.enabled = logic.getIsPlayerTurn()
        viewScoresButton.enabled = logic.getIsPlayerTurn()
    }
    
    private func reset() {
        logic.setTurnTotal(0)
        logic.setPlayerScore(0)
        logic.setComputerScore(0)
        turnTotal.text = String(NSLocalizedString("Turn Total : ", comment: "") + String(logic.getTurnTotal()))
        playerScore.text = String(logic.getPlayerName() + NSLocalizedString(" Score : ", comment: "") + String(logic.getPlayerScore()))
        computerScore.text = String(NSLocalizedString("Computer Score: ", comment: "") + String(logic.getComputerScore()))
    }
    
}

