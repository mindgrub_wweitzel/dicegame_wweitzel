//
//  Logic.swift
//  DiceGame
//
//  Created by Wesley Weitzel on 7/12/16.
//  Copyright © 2016 Wesley Weitzel. All rights reserved.
//

import Foundation

class Logic {
    
    private let GOAL_SCORE = 20
    private var turnTotal = 0
    private var playerScore = 0
    private var computerScore = 0
    
    private var isPlayerTurn = true
    
    private var playerName = "Player"
    
    func getPlayerName() -> String {
        return playerName
    }
    
    func getGoalScore() -> Int {
        return GOAL_SCORE
    }
    
    func getTurnTotal() -> Int {
        return turnTotal
    }
    
    func getPlayerScore() -> Int {
        return playerScore
    }
    
    func getComputerScore() -> Int {
        return computerScore
    }
    
    func setTurnTotal(value: Int) {
        turnTotal = value
    }
    
    func setPlayerScore(newPlayerScore: Int) {
        playerScore = newPlayerScore
    }
    
    func setComputerScore(newComputerScore: Int) {
        computerScore = newComputerScore
    }
    
    func getIsPlayerTurn() -> Bool {
        return isPlayerTurn
    }
    
    func setIsPlayerTurn(isPlayerTurn: Bool) {
        self.isPlayerTurn = isPlayerTurn
    }
    
    
    
}