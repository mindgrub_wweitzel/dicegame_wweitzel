//
//  ScoresTableViewController.swift
//  DiceGame
//
//  Created by Wesley Weitzel on 6/30/16.
//  Copyright © 2016 Wesley Weitzel. All rights reserved.
//

import UIKit
import CoreData

class ScoresTableViewController: UITableViewController {
    
    var gameScores = [NSManagedObject]()
    
    @IBOutlet weak var computerScoreLabel: UILabel!
    @IBOutlet weak var playerScoreLabel: UILabel!
    private let entityName = "Scores"
    private let cellName = "Cell"
    private let pScoreKey = "pScore"
    private let cScoreKey = "cScore"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerScoreLabel.text = NSLocalizedString("Player Score:", comment: "")
        computerScoreLabel.text = NSLocalizedString("Computer Score", comment: "")
        fetchScores()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
    }
    
    func fetchScores() {
        let fetchRequest = NSFetchRequest(entityName: entityName)
        // Initialize Asynchronous Fetch Request
        let asynchronousFetchRequest = NSAsynchronousFetchRequest(fetchRequest: fetchRequest) { (asynchronousFetchResult) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.processAsynchronousFetchResult(asynchronousFetchResult)
            })
        }
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        do {
            // Execute Asynchronous Fetch Request
            let asynchronousFetchResult = try managedContext.executeRequest(asynchronousFetchRequest)
            print(asynchronousFetchResult)
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }

    }
    
    func processAsynchronousFetchResult(asynchronousFetchResult: NSAsynchronousFetchResult) {
        if let result = asynchronousFetchResult.finalResult {
            // Update gameScores
            gameScores = result as! [NSManagedObject]
            // Reload Table View
            tableView.reloadData()
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameScores.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellName, forIndexPath: indexPath) as! CustomCell
        let score = gameScores[indexPath.row]
        cell.playerScore.text = score.valueForKey(pScoreKey) as? String
        cell.computerScore.text = score.valueForKey(cScoreKey) as? String
        return cell
    }
}
