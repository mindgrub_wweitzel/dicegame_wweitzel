//
//  ScoresHandler.swift
//  DiceGame
//
//  Created by Wesley Weitzel on 7/12/16.
//  Copyright © 2016 Wesley Weitzel. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ScoresHandler {
    
    var gameScores = [NSManagedObject]()
    private let entityName = "Scores"
    private let pScoreName = "pScore"
    private let cScoreName = "cScore"
    private let errorMessage = "error @ saveItem"
    
    func saveItem(playerScore : String, compScore : String) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let gameScoresEntity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: managedContext)
        let score = NSManagedObject(entity: gameScoresEntity!, insertIntoManagedObjectContext: managedContext)
        score.setValue(playerScore, forKey: pScoreName)
        score.setValue(compScore, forKey: cScoreName)
        do {
            try managedContext.save()
            gameScores.append(score)
        } catch {
            print(errorMessage)
        }
    }
    
    
    
}